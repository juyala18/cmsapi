package org.akhilesh.poc.cmsapi.aws;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class AWSUtils {
    @Autowired
    private AmazonS3 s3;
    public boolean whenVerifyingIfS3BucketExist_thenCorrect(String bucket) {
        System.out.println("list of available buckets: " + s3.listBuckets());
        return s3.doesBucketExist(bucket);
    }

    public  String getObjectFromS3(String bucket, String path) throws IOException {
        String line;
        String line1 = "";
        //downloading an object
        S3Object s3object = s3.getObject(new GetObjectRequest(bucket, path));//path = testrole/democode.txt
        S3ObjectInputStream inputStream = s3object.getObjectContent();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        while ((line = reader.readLine()) != null) {
            line1+=line;
        }
        return line1;
    }

    public String getJSON(String objectFromS3){
        String message;
        JSONObject json = new JSONObject();
        json.put("name", "cms");


        JSONObject item = new JSONObject();
        item.put("content", objectFromS3);


        json.put("value", item);

        message = json.toString();
        return message;

    }
}
