package org.akhilesh.poc.cmsapi.api;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import org.akhilesh.poc.cmsapi.aws.AWSS3Service;
import org.akhilesh.poc.cmsapi.aws.AWSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.*;


@RestController
@RequestMapping("/aws")
public class ContentManagerController {
    @Autowired
    private AmazonS3 s3;
    @Autowired
    private AWSUtils awsUtils;

    private AWSS3Service service ;

    private static final String BUCKET_NAME = "akkij94rest";
    private static final String KEY_NAME = "key_name";
    private static final String BUCKET_NAME2 = "bucket_name2";
    private static final String KEY_NAME2 = "key_name2";
    @PostConstruct
    private void init() {
        service = new AWSS3Service(s3);
    }

    @RequestMapping(value = "/verify/{bucket}")
    public String getAWSContent(@PathVariable String bucket){

        if(awsUtils.whenVerifyingIfS3BucketExist_thenCorrect(bucket)) {
            return "pass";
        } else {
            return "fail";
        }

    }

    @RequestMapping(value = "/download/{bucket}/{path}")
    @ResponseBody
    public String getdownload(@PathVariable String bucket, @PathVariable String path) throws IOException {
        if (path.contains("_")) {
            path=path.replaceAll("_", "\\/");
        }
        String message=awsUtils.getJSON(awsUtils.getObjectFromS3(bucket, path));
        return message;
    }

}
